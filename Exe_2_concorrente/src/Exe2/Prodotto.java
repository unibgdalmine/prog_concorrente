package Exe2;

import java.util.ArrayDeque;
import java.util.Scanner;

public class Prodotto {			
	private ArrayDeque<Integer> buffer=new ArrayDeque<>();
	private final int max_size=10;
	
	
	public synchronized void addNumb(){	
		 
			if(buffer.size()>=max_size){
				System.out.println("Produttore: Waiting");
				try {	
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				Scanner in=new Scanner(System.in);
				System.out.println("Inserire numero nuovo");
				buffer.addLast(in.nextInt());
				System.out.println("Produttore: NOTIFY");
				notify();
		 
		
		
		
	}
	
	public synchronized Integer readNumb(){	
		int res;
			
			if(buffer.isEmpty()){
				System.out.println("Consumer: Waiting");
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
				res=buffer.getLast();
				buffer.removeLast();
				System.out.println("Consumer: NOTIFY");	
				
				
	
			
		notify();
		return res;
	}
	
	public static void main(String[] args){
		Prodotto nuovo=new Prodotto();
		int i=0;
		Scanner in=new Scanner(System.in);
		System.out.println("Inserire numero produttori");
		int N=in.nextInt();
		System.out.println("Inserire numero consumatori");
		int M=in.nextInt();
		
		
			for(int j=0;j<M;j++)
			new consumer(nuovo).start();
			for(int j=0;j<N;j++)
			nuovo.addNumb();
			i++;
		
	}
}

	
	class consumer extends Thread{
		Prodotto nuovo=new Prodotto();
		
		
		public consumer(Prodotto nuovo){
			this.nuovo=nuovo;
		}


		@Override
		public void run() {
			System.out.println(nuovo.readNumb().toString());
		}
		
		
	}

